## Артефакт - 1. Детализация и четкое прописывание бизнес-целей

1. Создание и увеличение саморазвивающегося комьюнити, лояльного к бренду и к получению рекламной информации
о бренде и товарах.
2. Увеличить продажи за счет стимулирования спроса (промоакций). Анонсировать появление новинок. Рассказывать 
об истории бренда, наших конкурентных преимуществах, о продаваемых товарах.


## Артефакт - 2. Анализ и список функциональных требований

1. Автоматическое формирование Групп по интересам. Для этого нужно чтобы Пользователь при регистрации заполнил анкету.
Примеры групп:
- Худеющие
- Набирающие массу
- Любители бега
- Начинающие бегать
- Готовящиеся к марафону
2. Внутри Групп Пользователи общаются, переписываются, создают темы для обсуждений, оставляют комментарии.
3. Возможность поиска людей: по интересам, по местам тренировок, по характеристикам, онлайн/офлайн.
4. Пользователи могут обмениваться личными сообщениями, подписываться на результаты тренировок друг друга. Пользователь
может выбрать, делиться ему информацией о своих успехах или нет.
5. Пользователи могут создавать ивенты для совместных тренировок и приглашать других.
6. Приложение информирует Пользователей о предстоящих соревнованиях, совместных тренировках.
6. Пользователь может вести индивидуальный дневник с целями и рефлексией по достижению целей.
7. Пользователь может указать какой у него есть спорт инвентарь и на основе этого получать рекомендации по программам тренировок.
8. Приложение помогает Пользователям составлять программы тренировок и расписаний с индивидуальными рекомендациями 
в зависимости от целей.
9. Приложение позволяет Пользователям анализировать свои результаты: тренд, прогресс, достижения, а также сравнивать
их с результатами других Пользователей.
10. После запуска Приложения и накопления достаточного объема информации в Приложении появится матрица ачивок,
достижений Пользователей. Ачивки формируются по видам спорта и в зависимости от уровня Пользователя.
11. Приложение собирает и обрабатывает информацию о передвижении Пользователей, скорости его движения (например для велотренировок).
12. Приложение можно синтегрировать с фитнес функциями телефона для сбора и отображения данных: пульс, кислород, сколько калорий сжег и т.д.
13. Возможно подключение к Приложению умных устройств: умные гантели, умный мяч футбольный / баскетбольный, умная 
ракетка для тенниса, перчатка для гольфа, очки для велотренировок и т.д. 
Собираемая информация позволит Пользователям эффективнее развивать свои навыки.
14. В случае согласия Пользователей на получение уведомлений, Приложение отправляет информацию о необходимости обновить инвентарь,
либо если появилась новая модель, получает информацию о ней.
15. В Приложении создан сервис для наших маркетологов, который помогает запускать промоакции на товары, отправлять 
информацию о компании, новинках, рассказывать о наших преимуществах, а также выбирать сегменты аудиторий для отправки - 
по месту жительства, виду спорта, полу Пользователей, опыту и т.д.
16. Если Пользователь заинтересовался в определенном виде товара, он переходит в Приложение онлайн-покупок, в котором автоматически
подтягивается информация о Пользователе и нужном ему товаре.
17. Приложение для покупки получает информацию из нашего Приложения для персональных рекомендаций.
18. Рейтинговая система. Для каждого Пользователя расчитывается личный рейтинг по результатам тренировок.

## Артефакт - 3. Анализ стейкхолдеров и их интересов

1. **Начинающие Спортсмены:**
_Те кто только начинают делать первые шаги._
- Нужна мотивация для занятий спортом, нужна поддержка.
- Нужно помогать с самоорганизацией: планировать время и место тренировки, ее продолжительность, напоминать заранее, 
сообщать о том что нужно (какой инвентарь) для тренировки.
- Нужны друзья-единомышленники по интересам. При появлении друзей на тренировках, для начинающего спортсмена 
больше мотивация для занятий.
- Хотят работать над собой, своим телом и характером, поэтому важно видеть результаты своей деятельности, что есть
эффект, результаты.
- Важно получать удовольствие от тренировок.
- Важно беречь здоровье, укреплять его.
- Хотят улучшать качество жизни: хорошее самочувствие и здоровое тело влияют на достижения в обычной жизни.

2. **Любители Спортсмены:**
_Те кто уже занимался, не менее 6 месяцев подряд._
- Нужна мотивация для продолжения занятий спортом.
- Важно видеть результаты своей деятельности.
- Важно наличие поддерживающего сообщества.
- Важно беречь здоровье, укреплять его.
- Хотят иметь возможность общаться с более опытными Спортсменами.
- Важно быть вкурсе новых трендов, событий, новых спортивных возможностей.

3. **Профессиональные Спортсмены:**
_Те кто живут спортом и зарабатывают на нём._
- Нужны спонсоры.
- Нужна информация для работы над своими навыками и улучшения своих результатов.
- Хотят быть лучше своих конкурентов.
- Важно понимать свой уровень, видеть свои слабые и сильные стороны, понимать на чем стоит сфокусироваться.
- Важно видеть результаты своей деятельности.
- Важно быть на связи с спорт сообществом, быть в курсе новых трендов, событий, новых спортивных возможностей.

4. **Наши маркетологи, продавцы:**
- Знать своих клиентов, их интересы.
- Нужна лояльная аудитория, готовая общаться, при желании проходить интервью, кастдевы и прочее.
- Рассказывать о новых товарах, рассказывать о компании, нужен инструмент влияния на клиентов.
